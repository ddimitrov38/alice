//
//  ViewController.m
//  Alice iOS
//
//  Created by Dimitar Dimitrov on 6/24/15.
//  Copyright (c) 2015 Dimitar Dimitrov. All rights reserved.
//

#import "ViewController.h"
#import "UIBubbleTableView.h"
#import "UIBubbleTableViewDataSource.h"
#import "NSBubbleData.h"

#include "std_util.h"
#include "aiml.h"

using namespace std;
using std_util::strip;
using namespace aiml;


@interface ViewController () {
	UIView *containerView;

	aiml::cInterpreter* alice;
	std::string result;
	std::list<aiml::cMatchLog> log;
}

@end

@implementation ViewController
@synthesize bubbleTable;
@synthesize textView;

- (id)init {
	self = [super init];

	if (self) {
		[self setTitle:@"A.L.I.C.E"];

		alice = cInterpreter::newInterpreter();

		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(keyboardWillShow:)
													 name:UIKeyboardWillShowNotification
												   object:nil];

		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(keyboardWillHide:)
													 name:UIKeyboardWillHideNotification
												   object:nil];


		bubbleTable = [[UIBubbleTableView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height - 80)];
		bubbleTable.contentInset = UIEdgeInsetsMake(0, 0, 20, 0);
		[bubbleTable setBubbleDataSource:self];
		[bubbleTable setBackgroundColor:[UIColor lightGrayColor]];
		// The line below sets the snap interval in seconds. This defines how the bubbles will be grouped in time.
		// Interval of 120 means that if the next messages comes in 2 minutes since the last message, it will be added into the same group.
		// Groups are delimited with header which contains date and time for the first message in the group.

		bubbleTable.snapInterval = 120;

		// The line below enables avatar support. Avatar can be specified for each bubble with .avatar property of NSBubbleData.
		// Avatars are enabled for the whole table at once. If particular NSBubbleData misses the avatar, a default placeholder will be set (missingAvatar.png)

		bubbleTable.showAvatars = YES;

		// Uncomment the line below to add "Now typing" bubble
		// Possible values are
		//    - NSBubbleTypingTypeSomebody - shows "now typing" bubble on the left
		//    - NSBubbleTypingTypeMe - shows "now typing" bubble on the right
		//    - NSBubbleTypingTypeNobody - no "now typing" bubble

		bubbleTable.typingBubble = NSBubbleTypingTypeNobody;

		[self.view addSubview:bubbleTable];


//		NSBubbleData *photoBubble = [NSBubbleData dataWithImage:[UIImage imageNamed:@"halloween.jpg"] date:[NSDate dateWithTimeIntervalSinceNow:-290] type:BubbleTypeSomeoneElse];
//		photoBubble.avatar = [UIImage imageNamed:@"missingAvatar.png"];
		NSBubbleData *greetingsBubble = [NSBubbleData dataWithText:@"Hi, please wait I'm loading my data..." date:[NSDate date] type:BubbleTypeSomeoneElse];
		greetingsBubble.avatar = [UIImage imageNamed:@"alice.png"];

		bubbleData = [NSMutableArray arrayWithObjects:greetingsBubble, nil];


		// Text field
		containerView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 40, 320, 40)];

		textView = [[HPGrowingTextView alloc] initWithFrame:CGRectMake(6, 3, 240, 40)];
		textView.isScrollable = NO;
		textView.contentInset = UIEdgeInsetsMake(0, 5, 0, 5);
		textView.minNumberOfLines = 1;
		textView.maxNumberOfLines = 1;
		textView.returnKeyType = UIReturnKeyGo;
		textView.font = [UIFont systemFontOfSize:15.0f];
		[textView setDelegate:self];
		textView.internalTextView.scrollIndicatorInsets = UIEdgeInsetsMake(5, 0, 5, 0);
		textView.backgroundColor = [UIColor whiteColor];
		textView.placeholder = @"Type your message here";

		UIImage *rawEntryBackground = [UIImage imageNamed:@"MessageEntryInputField.png"];
		UIImage *entryBackground = [rawEntryBackground stretchableImageWithLeftCapWidth:13 topCapHeight:22];
		UIImageView *entryImageView = [[UIImageView alloc] initWithImage:entryBackground];
		entryImageView.frame = CGRectMake(5, 0, 248, 40);
		entryImageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;

		UIImage *rawBackground = [UIImage imageNamed:@"MessageEntryBackground.png"];
		UIImage *background = [rawBackground stretchableImageWithLeftCapWidth:13 topCapHeight:22];
		UIImageView *imageView = [[UIImageView alloc] initWithImage:background];
		imageView.frame = CGRectMake(0, 0, containerView.frame.size.width, containerView.frame.size.height);
		imageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;

		textView.autoresizingMask = UIViewAutoresizingFlexibleWidth;

		// view hierachy
		[containerView addSubview:imageView];
		[containerView addSubview:textView];
		[containerView addSubview:entryImageView];

		[self.view addSubview:containerView];

		containerView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
		UIImage *sendBtnBackground = [[UIImage imageNamed:@"MessageEntrySendButton.png"] stretchableImageWithLeftCapWidth:13 topCapHeight:0];
		UIImage *selectedSendBtnBackground = [[UIImage imageNamed:@"MessageEntrySendButton.png"] stretchableImageWithLeftCapWidth:13 topCapHeight:0];

		UIButton *doneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
		doneBtn.frame = CGRectMake(containerView.frame.size.width - 69, 8, 63, 27);
		doneBtn.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin;
		[doneBtn setTitle:@"Say" forState:UIControlStateNormal];

		[doneBtn setTitleShadowColor:[UIColor colorWithWhite:0 alpha:0.4] forState:UIControlStateNormal];
		doneBtn.titleLabel.shadowOffset = CGSizeMake (0.0, -1.0);
		doneBtn.titleLabel.font = [UIFont boldSystemFontOfSize:16.0f];

		[doneBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
		[doneBtn addTarget:self action:@selector(growingTextViewShouldReturn:) forControlEvents:UIControlEventTouchUpInside];
		[doneBtn setBackgroundImage:sendBtnBackground forState:UIControlStateNormal];
		[doneBtn setBackgroundImage:selectedSendBtnBackground forState:UIControlStateSelected];

		[containerView addSubview:doneBtn];
	}

	return self;
}

#pragma mark - UIBubbleTableViewDataSource methods
- (NSInteger)rowsForBubbleTable:(UIBubbleTableView *)tableView {
	return [bubbleData count];
}

- (NSBubbleData *)bubbleTableView:(UIBubbleTableView *)tableView dataForRow:(NSInteger)row {
	return [bubbleData objectAtIndex:row];
}

#pragma mark - HPGrowingTextViewDelegate methods
- (BOOL)growingTextView:(HPGrowingTextView *)growingTextView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
	if([text isEqualToString:@"\n"]) {
		[self growingTextViewShouldReturn:growingTextView];
		return NO;
	}

	return YES;
}

std::string Translit(const std::string& src) {
	std::string Res;
	std::string Rus = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЭЮЯабвгдеёжзийклмнопрстуфхцчшщьыъэюя";
	const std::string Lat[] = {"A", "B", "V", "G", "D", "E", "E", "Zh", "Z", "I", "I", "K",
		"L", "M", "N", "O", "P", "R", "S", "T", "U", "F", "H", "C", "Ch", "Sh", "Sh'", "E",
		"Yu", "Ya", "a", "b", "v", "g", "d", "e", "e", "zh", "z", "i", "i", "k", "l", "m", "n",
		"o", "p", "r", "s", "t", "u", "f", "h", "c", "ch", "sh", "sh'", "'", "i", "'", "e", "yu",
		"ya"};

	for (std::string::const_iterator i = src.begin() ; i != src.end() ; ++i) {
		std::string::size_type Pos = Rus.find(*i);
		if (Pos != std::string::npos)
			Res += Lat[Pos];
		else {
			if (*i < 0)
				Res += ' ';
			else
				Res += *i;
		}
	}

	return Res;
}


-(BOOL) isCyrillic:(NSString*) text {
	NSError *error = NULL;

	NSRegularExpression *regex = [NSRegularExpression
								  regularExpressionWithPattern:@"|[А-З]|i"
								  options:0
								  error:&error];

	return ([regex matchesInString:text options:0 range:NSMakeRange(0, text.length)] > 0);
}

- (BOOL)growingTextViewShouldReturn:(HPGrowingTextView *)growingTextView {

	if ([textView.text  isEqual:@""]) {
		[textView resignFirstResponder];
		return YES;
	}


	if ([self isCyrillic:textView.text]) {
		NSLog(@"IS Cyrillic");
	} else
		NSLog(@"NOT Cyrillic");

	NSBubbleData *sayBubble = [NSBubbleData dataWithText:textView.text date:[NSDate date] type:BubbleTypeMine];
	sayBubble.avatar = [UIImage imageNamed:@"user.jpg"];

	[bubbleData addObject:sayBubble];
	[bubbleTable reloadData];


	if (alice->respond(textView.text.UTF8String, "localhost", result, &log)) {
		NSBubbleData *aliceBubble = [NSBubbleData dataWithText:[NSString stringWithUTF8String:strip(result).c_str()] date:[NSDate date] type:BubbleTypeSomeoneElse];
		aliceBubble.avatar = [UIImage imageNamed:@"alice.png"];

		[bubbleData addObject:aliceBubble];
		[bubbleTable reloadData];
	} else {
		NSBubbleData *errorBubble = [NSBubbleData dataWithText:[NSString stringWithFormat:@"Error: %s", alice->getErrorStr(alice->getError()).c_str()] date:[NSDate date] type:BubbleTypeSomeoneElse];
		errorBubble.avatar = [UIImage imageNamed:@"misingAvatar.png"];

		[bubbleData addObject:errorBubble];
		[bubbleTable reloadData];

		NSLog(@"Error: %@", [NSString stringWithFormat:@"%s", strip(result).c_str()]);
	}

	// Scroll to last row
	NSIndexPath* lastRowIndexPath = [NSIndexPath indexPathForRow:[bubbleTable numberOfRowsInSection:[bubbleTable numberOfSections] - 1] -1 inSection:[bubbleTable numberOfSections] - 1];
	[bubbleTable scrollToRowAtIndexPath:lastRowIndexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];

	textView.text = @"";
	[textView resignFirstResponder];

	return YES;
}


- (void)growingTextView:(HPGrowingTextView *)growingTextView willChangeHeight:(float)height {
	float diff = (growingTextView.frame.size.height - height);

	CGRect r = containerView.frame;
	r.size.height -= diff;
	r.origin.y += diff;
	containerView.frame = r;
}



#pragma mark Keyboard events
//Code from Brett Schumann
-(void) keyboardWillShow:(NSNotification *)note{
	// get keyboard size and loctaion
	CGRect keyboardBounds;
	[[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
	NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
//	NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];

	// Need to translate the bounds to account for rotation.
	keyboardBounds = [self.view convertRect:keyboardBounds toView:nil];

	// get a rect for the textView frame
	CGRect containerFrame = containerView.frame;
	containerFrame.origin.y = self.view.bounds.size.height - (keyboardBounds.size.height + containerFrame.size.height);
	// animations settings
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationDuration:[duration doubleValue]];
	[UIView setAnimationCurve:UIViewAnimationCurveEaseOut];

	// set views with new info
	containerView.frame = containerFrame;

	// commit animations
	[UIView commitAnimations];
}

-(void) keyboardWillHide:(NSNotification *)note{
	NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];

	// get a rect for the textView frame
	CGRect containerFrame = containerView.frame;
	containerFrame.origin.y = self.view.bounds.size.height - containerFrame.size.height;

	// animations settings
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationDuration:[duration doubleValue]];
	[UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
	// set views with new info
	containerView.frame = containerFrame;

	// commit animations
	[UIView commitAnimations];
}



- (void)viewDidLoad {
	[super viewDidLoad];

	// TODO:: Add loading indication

	NSString *bundlePath = [[NSBundle mainBundle] resourcePath];

	dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
		cCoreOptions opts;

		opts.file_gossip = [NSString stringWithFormat:@"%@/gossip.txt", bundlePath].UTF8String;
		opts.file_patterns = [[NSString stringWithFormat:@"%@/*.aiml", bundlePath] stringByReplacingOccurrencesOfString:@" " withString:@"\\ "].UTF8String
		;
		opts.user_file = [NSString stringWithFormat:@"%@/userlist.xml", bundlePath].UTF8String;
		opts.sentence_limit = "?:!:.:;:ï¼Ÿ:ï¼:ã€‚:ï¼›";
		opts.should_trim_blanks = true;
		opts.allow_javascript = false;
		opts.allow_system = false;

		if (!alice->initialize([NSString stringWithFormat:@"%@/libaiml.xml", bundlePath].UTF8String, opts)) {
			NSLog(@"Could not load config data: %s", alice->getErrorStr(alice->getError()).c_str());
		} else {
			dispatch_async(dispatch_get_main_queue(), ^(void){
				NSBubbleData *heyBubble = [NSBubbleData dataWithText:@"Ok, I'm ready, thaks for waiting. You can now ask me something." date:[NSDate date] type:BubbleTypeSomeoneElse];
				heyBubble.avatar = [UIImage imageNamed:@"alice.png"];

				[bubbleData addObject:heyBubble];
				[bubbleTable reloadData];
			});

			NSLog(@"Config ok!");
		}
	});
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
}

@end
