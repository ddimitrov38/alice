#include <iostream>
#include "stdio.h"
#include "stdlib.h"
#include "string.h"

#include "ofxEliza.h"

int main(int ac, char **av) {
	ofxEliza eliza;
	std::string userSays;

	try {
		eliza.init("script.txt", "log.txt");
	} catch (std::string e) {
		printf("Catched exception: %s!", e.c_str());

		return -1;
	}

	while (42) {
		printf("\n> ");
		std::getline(std::cin, userSays);
		printf("%s", eliza.ask(userSays).c_str());

		if (userSays == "") {
			printf("Bye!\n");
			break;
		}
	}

	return 0;
}
