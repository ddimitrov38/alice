//
//  main.m
//  Alice iOS
//
//  Created by Dimitar Dimitrov on 6/24/15.
//  Copyright (c) 2015 Dimitar Dimitrov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
	}
}
