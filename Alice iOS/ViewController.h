//
//  ViewController.h
//  Alice iOS
//
//  Created by Dimitar Dimitrov on 6/24/15.
//  Copyright (c) 2015 Dimitar Dimitrov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIBubbleTableViewDataSource.h"
#import "HPGrowingTextView.h"

@class HPGrowingTextView;

@interface ViewController : UIViewController <UIBubbleTableViewDataSource, HPGrowingTextViewDelegate> {
	NSMutableArray *bubbleData;
}

@property (nonatomic, retain) HPGrowingTextView *textView;
@property (nonatomic, retain) UIBubbleTableView *bubbleTable;

@end

